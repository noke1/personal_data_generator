package provider;

import com.github.javafaker.Faker;
import pojo.BirthdayDetails;

import java.time.LocalDate;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

public class BirthDayDetailsProvider {

    private final Faker faker;

    public BirthDayDetailsProvider() {
        this.faker = new Faker(new Locale("pl"));
    }

    public BirthdayDetails generateData(){
        return new BirthdayDetails(
                generateDateOfBirth(),
                generatePlaceOfBirth()
        );
    }


    /**
     * Generate random date of birth of adult person.
     * @return
     */
    private LocalDate generateDateOfBirth() {
        final long todayMinus118YearsInMillis = LocalDate.now().minusYears(118).toEpochDay();
        final long todayMinus18YearsInMillis = LocalDate.now().minusYears(18).minusDays(1).toEpochDay();

        final long randomMillisInRange = ThreadLocalRandom.current()
                .nextLong(todayMinus118YearsInMillis,todayMinus18YearsInMillis);

        return LocalDate.ofEpochDay(randomMillisInRange);
    }

    private String generatePlaceOfBirth(){
        return faker.address().city();
    }

}
