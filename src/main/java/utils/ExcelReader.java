package utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class ExcelReader {

    private static final String FILE_PATH = "src/main/resources/Polish_zipCodes_list_2018.xlsx";


    public String getRandomZipCode() {
        final int minValue = 2;
        final int maxValue = 4005;
        final int sheetPageNumber = 0;
        final int randomNumber = ThreadLocalRandom.current().nextInt(minValue,maxValue + 1);
        final int cellNumber = 0;
        DataFormatter dataFormatter = new DataFormatter();

        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new File(FILE_PATH));
        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheetAt(sheetPageNumber);
        Row row = sheet.getRow(randomNumber);
        Cell cell = row.getCell(cellNumber);
        String cellValue = dataFormatter.formatCellValue(cell);

        return cellValue;
    }


}
