package pesel_provider;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pojo.Pesel;
import provider.PeselProvider;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static pojo.userenum.Gender.FEMALE;
import static pojo.userenum.Gender.MALE;

public class PeselProviderTest {

    private PeselProvider  peselProvider;

    @Test(groups = {"positive", "yearValidation"})
    public void shouldGeneratePeselWith11DigitsWhenCorrectDataProvided(){
        peselProvider = new PeselProvider(LocalDate.parse("1993-10-30"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as("Pesel length is equal to 11, when correct data is provided")
                .extracting(Pesel::getPesel)
                .extracting(String::length)
                .isEqualTo(11);
    }

    @Test(groups = {"positive", "yearValidation", "boundaryValue"})
    public void shouldPassWhenLowerBoundaryYearProvided(){
        peselProvider = new PeselProvider(LocalDate.parse("1801-10-30"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as("Pesel length is not equal to 11, when correct data is provided")
                .extracting(Pesel::getPesel)
                .extracting(String::length)
                .isEqualTo(11);
    }

    @Test(groups = {"negative", "yearValidation"})
    public void shouldThrowExceptionWhenYearBelowLowerBoundaryProvided(){
        peselProvider = new PeselProvider(LocalDate.parse("1799-10-30"), FEMALE);

        Throwable thrown = catchThrowable(() -> peselProvider.generateNumber());

        assertThat(thrown)
                .as("Check if year 1800 is not accepted to create PESEL from. 1800 is one year below the lower boundary.")
                .hasStackTraceContaining("java.lang.IllegalArgumentException");
    }


    @Test(groups = {"positive", "yearValidation", "boundaryValue"})
    public void shouldPassWhenUpperBoundaryYearProvided(){
        peselProvider = new PeselProvider(LocalDate.parse("2299-10-30"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as("Pesel length is not equal to 11, when correct data is provided")
                .extracting(Pesel::getPesel)
                .extracting(String::length)
                .isEqualTo(11);
    }


    @Test(groups = {"negative", "yearValidation"})
    public void shouldThrowExceptionWhenYearAboveUpperBoundaryProvided(){
        peselProvider = new PeselProvider(LocalDate.parse("2300-10-30"), FEMALE);

        Throwable thrown = catchThrowable(() -> peselProvider.generateNumber());

        assertThat(thrown)
                .as("Check if year 1800 is not accepted to create PESEL from. 1800 is one year below the lower boundary.")
                .hasStackTraceContaining("java.lang.IllegalArgumentException");
    }



    @DataProvider
    private Object[][] getYears(){
        return new Object[][]{
                {LocalDate.parse("1800-01-30"), "01", "80", "81"},
                {LocalDate.parse("1800-12-30"), "12", "80", "92"},
                {LocalDate.parse("1899-01-30"), "01", "80", "81"},
                {LocalDate.parse("1899-12-30"), "12", "80", "92"},

                {LocalDate.parse("1900-01-30"), "01", "0", "01"},
                {LocalDate.parse("1900-12-30"), "12", "0", "12"},
                {LocalDate.parse("1999-01-30"), "01", "0", "01"},
                {LocalDate.parse("1999-12-30"), "12", "0", "12"},

                {LocalDate.parse("2000-01-30"), "01", "20", "21"},
                {LocalDate.parse("2000-12-30"), "12", "20", "32"},
                {LocalDate.parse("2099-01-30"), "01", "20", "21"},
                {LocalDate.parse("2099-12-30"), "12", "20", "32"},

                {LocalDate.parse("2100-01-30"), "01", "40", "41"},
                {LocalDate.parse("2100-12-30"), "12", "40", "52"},
                {LocalDate.parse("2199-01-30"), "01", "40", "41"},
                {LocalDate.parse("2199-12-30"), "12", "40", "52"},

                {LocalDate.parse("2200-01-30"), "01", "60", "61"},
                {LocalDate.parse("2200-12-30"), "12", "60", "72"},
                {LocalDate.parse("2299-01-30"), "01", "60", "61"},
                {LocalDate.parse("2299-12-30"), "12", "60", "72"},
        };
    }

    @Test(groups = {"positive", "monthValidation", "boundaryValue"}, dataProvider = "getYears")
    public void shouldAddSpecificValueToMonthWhenSpecificYearProvided(LocalDate dateOfBirth, String month, String valueToAdd, String expectedValue){
        peselProvider = new PeselProvider(dateOfBirth, FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as(String.format("When year %s provided month should be %s + %s = %s",
                                String.valueOf(dateOfBirth).split("-")[0], //year
                                month,
                                valueToAdd,
                                expectedValue))
                .extracting(Pesel::getPesel)
                .extracting(s -> s.substring(2,4))
                .isEqualTo(expectedValue);
    }


    @DataProvider
    private Object[][] getMonthsBelow10(){
        return new Object[][]{
                {LocalDate.parse("1900-01-30"), "01", "0", "01"},
                {LocalDate.parse("1900-09-30"), "09", "0", "09"}
        };
    }

    @Test(groups = {"positive", "monthValidation", "boundaryValue"}, dataProvider = "getMonthsBelow10")
    public void shouldAddZeroToMonthWhenMonthBelow10Provided(LocalDate dateOfBirth, String month, String valueToAdd, String expectedValue){
        peselProvider = new PeselProvider(dateOfBirth, FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as(String.format("When month %s provided month should be %s + %s = %s",
                        month, //year
                        month,
                        valueToAdd,
                        expectedValue))
                .extracting(Pesel::getPesel)
                .extracting(s -> s.substring(2,4))
                .isEqualTo(expectedValue);
    }

    @DataProvider
    private Object[][] getMonthsAbove10(){
        return new Object[][]{
                {LocalDate.parse("1900-10-30"), "10", "0", "10"},
                {LocalDate.parse("1900-12-30"), "12", "0", "12"}
        };
    }

    @Test(groups = {"positive", "monthValidation", "boundaryValue"}, dataProvider = "getMonthsAbove10")
    public void shouldNotAddZeroToMonthWhenMonthAbove10Provided(LocalDate dateOfBirth, String month, String valueToAdd, String expectedValue){
        peselProvider = new PeselProvider(dateOfBirth, FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as(String.format("When month %s provided month should be %s + %s = %s",
                        month,
                        month,
                        valueToAdd,
                        expectedValue))
                .extracting(Pesel::getPesel)
                .extracting(s -> s.substring(2,4))
                .isEqualTo(expectedValue);
    }

    @Test(groups = {"positive", "dayValidation"})
    public void shouldAdd0WhenDayOfMonth1Provided(){
        peselProvider = new PeselProvider(LocalDate.parse("1993-10-01"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as("Lower boundary 01 should be 01 and be accepted")
                .extracting(Pesel::getPesel)
                .extracting(s -> s.substring(4,6))
                .isEqualTo("01");
    }

    @Test(groups = {"positive", "dayValidation"})
    public void shouldAdd0WhenDayOfMonth9Provided(){
        peselProvider = new PeselProvider(LocalDate.parse("1993-10-09"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();

        assertThat(expectedPesel)
                .as("Upper boundary 31 should be 31 and be accepted")
                .extracting(Pesel::getPesel)
                .extracting(s -> s.substring(4,6))
                .isEqualTo("09");
    }

    @Test(groups = {"positive", "genderNumberValidation"})
    public void shouldGenerateCorrectFemaleNumberWhenGenderFemaleProvided(){
        final List<Integer> FEMALE_NUMBERS = Arrays.asList(0,2,4,6,8);
        peselProvider = new PeselProvider(LocalDate.parse("1993-10-30"), FEMALE);

        Pesel expectedPesel = peselProvider.generateNumber();
        String expectedGenderNumber = expectedPesel.getPesel().substring(9,10);

        assertThat(FEMALE_NUMBERS)
                .as(String.format("Generated gender number %s should be present inside FEMALE_NUMBERS", expectedGenderNumber))
                .contains(Integer.parseInt(expectedGenderNumber));
    }

    @Test(groups = {"positive", "genderNumberValidation"})
    public void shouldGenerateCorrectMaleNumberWhenGenderMaleProvided(){
        final List<Integer> MALE_NUMBERS = Arrays.asList(1,3,5,7,9);
        peselProvider = new PeselProvider(LocalDate.parse("1993-10-30"), MALE);

        Pesel expectedPesel = peselProvider.generateNumber();
        String expectedGenderNumber = expectedPesel.getPesel().substring(9,10);

        assertThat(MALE_NUMBERS)
                .as(String.format("Generated gender number %s should be present inside MALE_NUMBERS", expectedGenderNumber))
                .contains(Integer.parseInt(expectedGenderNumber));
    }



}
