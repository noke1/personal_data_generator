package provider.identifiers;

import com.github.javafaker.Faker;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PassportProvider implements Identifier {

    private final Faker faker;
    private static final int[] CHARACTER_WEIGHT;
    private static final Map<Character, Integer> LETTER_AND_VALUE;

    static {
        CHARACTER_WEIGHT = new int[]{7, 3, 0, 1, 7, 3, 1, 7, 3};
        LETTER_AND_VALUE = new HashMap<>();
        for (char i = 'A'; i <= 'Z'; i++) LETTER_AND_VALUE.put(i, i - 55);
    }

    public PassportProvider() {
        this.faker = new Faker(new Locale("pl"));
    }


    @Override
    public String generateNumber() {
        String randomLetters;
        String randomNumbers;
        int mod;
        do {
            //reset sum in case of next iteration
            int charactersSum = 0;
            randomLetters = faker.letterify("??").toUpperCase();
            randomNumbers = faker.numerify("#######");

            charactersSum += getLettersSum(randomLetters, charactersSum) + getNumbersSum(randomNumbers, charactersSum);

            mod = charactersSum % 10;

            randomNumbers = mod + randomNumbers.substring(1);

        } while (!(mod == Character.getNumericValue(randomNumbers.charAt(0))));

        String passportIdentifier = randomLetters + randomNumbers;

        return passportIdentifier;
    }

    private int getNumbersSum(String randomNumbers, int charactersSum) {
        for (int i = 0, j = 2; i < randomNumbers.length(); i++, j++) {
            char number = randomNumbers.charAt(i);
            charactersSum += Character.getNumericValue(number) * CHARACTER_WEIGHT[j];
        }
        return charactersSum;
    }

    private int getLettersSum(String randomLetters, int charactersSum) {
        for (int i = 0; i < randomLetters.length(); i++) {
            char letter = randomLetters.charAt(i);
            charactersSum += LETTER_AND_VALUE.get(letter) * CHARACTER_WEIGHT[i];
        }
        return charactersSum;
    }
}
