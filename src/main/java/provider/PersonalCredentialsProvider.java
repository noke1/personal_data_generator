package provider;

import com.github.javafaker.Faker;
import pojo.PersonalCredentials;
import pojo.userenum.Gender;

import java.util.Locale;

public class PersonalCredentialsProvider {

    private final Faker faker;
    private Gender gender;
    private final String name;
    private final String lastName;

    public PersonalCredentialsProvider(){
        this.faker = new Faker(new Locale("pl"));
        this.name = generateName();
        this.lastName = generateLastName();
        setGender(this.name);
    }

    public PersonalCredentials generateData() {
        return new PersonalCredentials(
                getName(),
                getLastName(),
                getGender()
        );
    }

    public Gender getGender(){
        return gender;
    }

    public void setGender(String name){
        if (name.endsWith("a")) this.gender = Gender.FEMALE;
        else this.gender = Gender.MALE;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    private String generateName(){
        String randomName;

        do {
            randomName = faker.name().firstName();
        } while (!randomName.endsWith("a"));

        return randomName;
    }

    private String generateLastName(){
        String randomLastName;
        do {
            randomLastName = faker.name().lastName();
        } while (!randomLastName.endsWith("a"));

        return randomLastName;
    }

}
