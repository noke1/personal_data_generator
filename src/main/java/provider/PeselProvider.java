package provider;

import com.github.javafaker.Faker;
import pojo.Pesel;
import pojo.userenum.Gender;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.concurrent.ThreadLocalRandom;

public class PeselProvider {

    private final LocalDate dateOfBirth;
    private final Gender gender;

    public PeselProvider(LocalDate dateOfBirth, Gender gender) {
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
    }

    //date format 2001-06-16
    public Pesel generateNumber() {
        StringBuilder sb = new StringBuilder();

        String year = getYear(dateOfBirth);
        sb.append(year);

        String month = getMonth(dateOfBirth);
        sb.append(month);

        String day = getDay(dateOfBirth);
        sb.append(day);

        String nextThreeRandomNumbers = getNextThreeRandomNumbers();
        sb.append(nextThreeRandomNumbers);

        String genderNumber = getGenderNumber(gender);
        sb.append(genderNumber);

        String controlNumber = getControlNumber(sb.toString());
        sb.append(controlNumber);

        return new Pesel(sb.toString());
    }

    private String getControlNumber(String peselWithOutControlNumber) {
        final int[] weight = {9,7,3,1,9,7,3,1,9,7};
        int sum = 0;

        for(int i = 0; i < weight.length; i++) sum+=(Character.getNumericValue(peselWithOutControlNumber.charAt(i))*weight[i]);
        final int controlNumber = sum % 10;

        return String.valueOf(controlNumber);
    }

    private String getGenderNumber(Gender gender) {
        final int[] femaleNumbers = {0,2,4,6,8};
        final int[] maleNumbers = {1,3,5,7,9};
        int genderNumber = 0;
        final int randomGenderPosition = ThreadLocalRandom.current().nextInt(0,5);

        if(gender == Gender.FEMALE) genderNumber = femaleNumbers[randomGenderPosition];
        else if(gender == Gender.MALE) genderNumber = maleNumbers[randomGenderPosition];

        return String.valueOf(genderNumber);
    }

    private String getNextThreeRandomNumbers() {
        Faker faker = new Faker();
        return String.valueOf(faker.numerify("###"));
    }

    private String getDay(LocalDate dateOfBirth) {
        return dateOfBirth.getDayOfMonth() <= 9 ? "0" + dateOfBirth.getDayOfMonth() : String.valueOf(dateOfBirth.getDayOfMonth());
    }

    private String getMonth(LocalDate dateOfBirth) {
        final int year = dateOfBirth.getYear();
        int month = dateOfBirth.getMonthValue();

        if(year >= 1800 && year <= 1899) month += 80;
        if(year >= 1900 && year <= 1999) month += 0;
        if(year >= 2000 && year <= 2099) month += 20;
        if(year >= 2100 && year <= 2199) month += 40;
        if(year >= 2200 && year <= 2299) month += 60;

        //i need to add 0 if month is below 9, so fe. 8 becomes 08
        return month > 9 ? String.valueOf(month) : "0" + month;
    }

    private String getYear(LocalDate dateOfBirth){
        if (dateOfBirth == null) throw new InputMismatchException("Year length is not equal to 4!");
        if (dateOfBirth.getYear() < 1800 || dateOfBirth.getYear() > 2299) throw new IllegalArgumentException("Provided year is not in specified range!");

        return String.valueOf(dateOfBirth.getYear()).substring(2);
    }

}