package identifier_provider;

import org.testng.annotations.Test;
import pojo.Identifier;
import pojo.userenum.IdentifierType;
import provider.IdentifierProvider;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static pojo.userenum.IdentifierType.PASSPORT;
import static pojo.userenum.IdentifierType.PERSONAL_IDENTIFIER;

public class IdentifierProviderTest {

    @Test(groups = {"positive", "passportValidation"})
    public void shouldGeneratePassportWhenPassportProvided() {
        IdentifierProvider identifierProvider = new IdentifierProvider();

        identifierProvider.setPersonalDocumentType(PASSPORT);
        Identifier identifier = identifierProvider.generateData();

        assertThat(identifier.getPersonalDocumentType()).isEqualTo(PASSPORT);
    }

    @Test(groups = {"positive", "personalIdentifierValidation"})
    public void shouldGeneratePersonalIdentifierWhenPersonalIdentifierProvided() {
        IdentifierProvider identifierProvider = new IdentifierProvider();

        identifierProvider.setPersonalDocumentType(PERSONAL_IDENTIFIER);
        Identifier identifier = identifierProvider.generateData();

        assertThat(identifier.getPersonalDocumentType()).isEqualTo(PERSONAL_IDENTIFIER);
    }

    @Test(groups = {"negative", "personalIdentifierValidation"},
            description = "This test is to handle case when any value is added to the " +
            "IdentifierType enum. If enum is added switch-case inside IdentifierProvider class should be handled also." +
            "However enum has to added at the end of the enum values, otherwise it wont work :)")
    public void shouldGenerateIdentifierWithLastPossibleIdentifierTypeProvided() {
        final IdentifierType[] IDENTIFIERS = IdentifierType.values();
        final int LAST_POSSIBLE_IDENTIFIER = IDENTIFIERS.length-1;
        IdentifierProvider identifierProvider = new IdentifierProvider();

        identifierProvider.setPersonalDocumentType(IDENTIFIERS[LAST_POSSIBLE_IDENTIFIER]);
        Identifier identifier = identifierProvider.generateData();

        assertThat(identifier)
                .as("Check if lastly added enum to IdentifierType enum was handled inside switch-case in IdentifierProvider class.")
                .isInstanceOf(Identifier.class);
    }

}


