package provider.identifiers;

public interface Identifier {
    String generateNumber();
}
