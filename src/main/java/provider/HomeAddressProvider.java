package provider;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import pojo.home_address.HomeAddress;
import utils.ExcelReader;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class HomeAddressProvider {

    public HomeAddress generateData(){
        final String zipCode = generateZipCode();
        return generateHomeAddressFromZipCode(zipCode);
    }

    private String generateZipCode(){
        ExcelReader zipCodesList = new ExcelReader();
        return zipCodesList.getRandomZipCode();
    }

    private HomeAddress generateHomeAddressFromZipCode(String zipCode){
        final String urlPath = "http://kodpocztowy.intami.pl/api/" + zipCode;

        JsonPath jsonPath = RestAssured.given()
                .accept("application/json")
                .when()
                .get(urlPath)
                .then()
                .assertThat()
                .extract().body().jsonPath();
        List<HomeAddress> possibleAddresses = jsonPath.getList("",HomeAddress.class);

        final int randomNumber = ThreadLocalRandom.current().nextInt(0,possibleAddresses.size()+1);

        return possibleAddresses.get(randomNumber);
    }
}
