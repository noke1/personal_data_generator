package pojo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pojo.userenum.IdentifierType;

public class Identifier {

    private IdentifierType identifierType;
    private String personalDocumentNumber;

    public Identifier() {
    }

    public Identifier(IdentifierType identifierType, String personalDocumentNumber) {
        this.identifierType = identifierType;
        this.personalDocumentNumber = personalDocumentNumber;
    }

    public IdentifierType getPersonalDocumentType() {
        return identifierType;
    }

    public void setPersonalDocumentType(IdentifierType identifierType) {
        this.identifierType = identifierType;
    }

    public String getPersonalDocumentNumber() {
        return personalDocumentNumber;
    }

    public void setPersonalDocumentNumber(String personalDocumentNumber) {
        this.personalDocumentNumber = personalDocumentNumber;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
