package provider;

import pojo.Identifier;
import pojo.userenum.IdentifierType;
import provider.identifiers.PassportProvider;
import provider.identifiers.PersonalIdentifierProvider;

import java.util.concurrent.ThreadLocalRandom;

public class IdentifierProvider {

    private IdentifierType identifierType;

    public IdentifierProvider(){
        this.identifierType = generatePersonalDocumentType();
    }

    public Identifier generateData() {
        return new Identifier(
                getPersonalDocumentType(),
                generatePersonalDocumentNumber()
        );
    }

    private IdentifierType generatePersonalDocumentType(){
        final IdentifierType[] documentTypes = IdentifierType.values();
        final int randomNumber = ThreadLocalRandom.current().nextInt(0, documentTypes.length);
        return documentTypes[randomNumber];
    }

    private String generatePersonalDocumentNumber(){
        identifierType = getPersonalDocumentType();

        switch (identifierType) {
            case PERSONAL_IDENTIFIER:
                PersonalIdentifierProvider personalIdentifierProvider = new PersonalIdentifierProvider();
                return personalIdentifierProvider.generateNumber();
            case PASSPORT:
                PassportProvider passportProvider = new PassportProvider();
                return passportProvider.generateNumber();
            default:
                throw new IllegalArgumentException("Provided identifier is not one of the possible values. Provided: " + identifierType);
        }
    }

    public IdentifierType getPersonalDocumentType() {
        return identifierType;
    }

    public void setPersonalDocumentType(IdentifierType identifierType) {
        this.identifierType = identifierType;
    }
}
