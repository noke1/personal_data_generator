package pojo.home_address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

@JsonPropertyOrder({
        "kod",
        "nazwa",
        "miejscowosc",
        "ulica",
        "numer",
        "gmina",
        "powiat",
        "wojewodztwo",
        "dzielnica",
        "numeracja"
})
public class HomeAddress {

    @JsonProperty("kod")
    private String zipCode;
    @JsonProperty("nazwa")
    private String name;
    @JsonProperty("miejscowosc")
    private String town;
    @JsonProperty("ulica")
    private String street;
    @JsonProperty("numer")
    private String homeNumber;
    @JsonProperty("gmina")
    private String community;
    @JsonProperty("powiat")
    private String county;
    @JsonProperty("wojewodztwo")
    private String voivodeship;
    @JsonProperty("dzielnica")
    private String district;
    @JsonProperty("numeracja")
    private List<Numbering> numbering = null;


    /**
     * No args constructor for use in serialization
     *
     */
    public HomeAddress() {
    }

    /**
     *
     * @param zipCode
     * @param street
     * @param homeNumber
     * @param county
     * @param voivodeship
     * @param community
     * @param numbering
     * @param town
     * @param district
     * @param name
     */
    public HomeAddress(String zipCode, String name, String town, String street, String homeNumber, String community, String county, String voivodeship, String district, List<Numbering> numbering) {
        super();
        this.zipCode = zipCode;
        this.name = name;
        this.town = town;
        this.street = street;
        this.homeNumber = homeNumber;
        this.community = community;
        this.county = county;
        this.voivodeship = voivodeship;
        this.district = district;
        this.numbering = numbering;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    @JsonProperty("kod")
    public String getZipCode() {
        return zipCode;
    }

    @JsonProperty("kod")
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @JsonProperty("nazwa")
    public String getName() {
        return name;
    }

    @JsonProperty("nazwa")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("miejscowosc")
    public String getTown() {
        return town;
    }

    @JsonProperty("miejscowosc")
    public void setTown(String town) {
        this.town = town;
    }

    @JsonProperty("ulica")
    public String getStreet() {
        return street;
    }

    @JsonProperty("ulica")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("gmina")
    public String getCommunity() {
        return community;
    }

    @JsonProperty("gmina")
    public void setCommunity(String community) {
        this.community = community;
    }

    @JsonProperty("powiat")
    public String getCounty() {
        return county;
    }

    @JsonProperty("powiat")
    public void setCounty(String county) {
        this.county = county;
    }

    @JsonProperty("wojewodztwo")
    public String getVoivodeship() {
        return voivodeship;
    }

    @JsonProperty("wojewodztwo")
    public void setVoivodeship(String voivodeship) {
        this.voivodeship = voivodeship;
    }

    @JsonProperty("dzielnica")
    public String getDistrict() {
        return district;
    }

    @JsonProperty("dzielnica")
    public void setDistrict(String district) {
        this.district = district;
    }

    @JsonProperty("numeracja")
    public List<Numbering> getNumbering() {
        return numbering;
    }

    @JsonProperty("numeracja")
    public void setNumbering(List<Numbering> numbering) {
        this.numbering = numbering;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}