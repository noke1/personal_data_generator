package pojo.home_address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@JsonPropertyOrder({
        "od",
        "do",
        "parzystosc"
})
public class Numbering {

    @JsonProperty("od")
    private String from;
    @JsonProperty("do")
    private String to;
    @JsonProperty("parzystosc")
    private String parity;

    /**
     * No args constructor for use in serialization
     *
     */
    public Numbering() {
    }

    /**
     *
     * @param from
     * @param to
     * @param parity
     */
    public Numbering(String from, String to, String parity) {
        super();
        this.from = from;
        this.to = to;
        this.parity = parity;
    }

    @JsonProperty("od")
    public String getFrom() {
        return from;
    }

    @JsonProperty("od")
    public void setFrom(String from) {
        this.from = from;
    }

    @JsonProperty("do")
    public String getDo() {
        return to;
    }

    @JsonProperty("do")
    public void setDo(String _do) {
        this.to = _do;
    }

    @JsonProperty("parzystosc")
    public String getParity() {
        return parity;
    }

    @JsonProperty("parzystosc")
    public void setParity(String parity) {
        this.parity = parity;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}

















