import pojo.*;
import pojo.home_address.HomeAddress;
import provider.*;
import provider.IdentifierProvider;

public class Main {

    public static void main(String[] args) {
        PersonalCredentials personalCredentials =  new PersonalCredentialsProvider().generateData();
        BirthdayDetails birthdayDetails = new BirthDayDetailsProvider().generateData();
        ParentsDetails parentsDetails = new ParentsDetailsProvider().generateData();
        Pesel pesel = new PeselProvider(
                birthdayDetails.getDateOfBirth(),
                personalCredentials.getGender()
        ).generateNumber();
        Identifier identifier = new IdentifierProvider().generateData();
        HomeAddress homeAddress = new HomeAddressProvider().generateData();

        User user = new User(
                personalCredentials,
                birthdayDetails,
                parentsDetails,
                pesel,
                identifier,
                homeAddress
        );

        System.out.println(user.toString());
    }

}
