package pojo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pojo.home_address.HomeAddress;

public class User {

    private PersonalCredentials personalCredentials;
    private BirthdayDetails birthdayDetails;
    private ParentsDetails parentsDetails;
    private Pesel pesel;
    private Identifier documentDetails;
    private HomeAddress homeAddress;


    public User() {
    }

    public User(PersonalCredentials personalCredentials, BirthdayDetails birthdayDetails, ParentsDetails parentsDetails, Pesel pesel, Identifier documentDetails, HomeAddress homeAddress) {
        this.personalCredentials = personalCredentials;
        this.birthdayDetails = birthdayDetails;
        this.parentsDetails = parentsDetails;
        this.pesel = pesel;
        this.documentDetails = documentDetails;
        this.homeAddress = homeAddress;
    }

    public Pesel getPesel() {
        return pesel;
    }

    public void setPesel(Pesel pesel) {
        this.pesel = pesel;
    }

    public PersonalCredentials getPersonalCredentials() {
        return personalCredentials;
    }

    public void setPersonalCredentials(PersonalCredentials personalCredentials) {
        this.personalCredentials = personalCredentials;
    }

    public BirthdayDetails getBirthdayDetails() {
        return birthdayDetails;
    }

    public void setBirthdayDetails(BirthdayDetails birthdayDetails) {
        this.birthdayDetails = birthdayDetails;
    }

    public ParentsDetails getParentsDetails() {
        return parentsDetails;
    }

    public void setParentsDetails(ParentsDetails parentsDetails) {
        this.parentsDetails = parentsDetails;
    }

    public Identifier getDocumentDetails() {
        return documentDetails;
    }

    public void setDocumentDetails(Identifier documentDetails) {
        this.documentDetails = documentDetails;
    }

    public HomeAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(HomeAddress homeAddress) {
        this.homeAddress = homeAddress;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}
