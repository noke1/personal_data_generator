# What's this?
It's a solution for a real problem of personal data generation which is necessary to perform functional testing. This Java app produces a valid credentials of non-existing person.
Spoil: There are some unit tests present - it's just to practice and get used to creating it.
This is an example of produced user:
```
{
  "personalCredentials": {
    "name": "Ofelia",
    "lastname": "Góra",
    "gender": "FEMALE"
  },
  "birthdayDetails": {
    "dateOfBirth": {
      "year": 1967,
      "month": "MAY",
      "monthValue": 5,
      "leapYear": false,
      "dayOfMonth": 30,
      "dayOfWeek": "TUESDAY",
      "dayOfYear": 150,
      "era": "CE",
      "chronology": {
        "calendarType": "iso8601",
        "id": "ISO"
      }
    }
  },
  "parentsDetails": {
    "fatherName": "Grzegorz",
    "motherName": "Anastazja",
    "motherLastName": "Kujawa"
  },
  "pesel": {
    "pesel": "67053034848"
  },
  "documentDetails": {
    "personalDocumentNumber": "TU2913112",
    "personalDocumentType": "PASSPORT"
  },
  "homeAddress": {
    "kod": "44-152",
    "nazwa": null,
    "miejscowosc": "Gliwice",
    "ulica": "Niezapominajki",
    "numer": null,
    "gmina": "Gliwice",
    "powiat": "Gliwice",
    "wojewodztwo": "śląskie",
    "dzielnica": null,
    "numeracja": []
  }
}
``` 

# How is it all created?
- personalCredentials:
    - `name` - randomly by [Java Faker](https://github.com/DiUS/java-faker)  
    - `lastname` - randomly by [Java Faker](https://github.com/DiUS/java-faker)
    - `gender` - generates MAN or WOMAN based on randomly generated name. Name fits the gender.
- birthdayDetails:
    - `dateOfBirth` - random date in range of 18 years from today to 118 years from today
    - `placeOfBirth` - randomly by [Java Faker](https://github.com/DiUS/java-faker)
- parentsDetails:
    - `fatherName` - randomly by [Java Faker](https://github.com/DiUS/java-faker)
    - `motherName` - randomly by [Java Faker](https://github.com/DiUS/java-faker)
    - `motherLastName` - randomly by [Java Faker](https://github.com/DiUS/java-faker)
- pesel:
    - `pesel` - based on previously generated `gender` and `dateofBirth` creates perfectly valid (but imaginary) pesel number
- documentDetails:
    - `identifierType` - random chance to generate PASSPORT or PERSONAL_IDENTIFIER
    - `perosnalDocumentNumber` - based on previously generated `identifierType` creates  perfectly valid (but imaginary) document number
- homeAddress:
    - it is created by choosing a random zip code from excel file and making a GET request to external API [http://kodpocztowy.intami.pl/](http://kodpocztowy.intami.pl/)
    
# How to run?

## Run via IntelliJ
1. Clone repository via `git clone`
2. Open project in IntelliJ
3. Just go into `Main.java` and run main method
4. The output will be visible inside IntelliJ console

## Run via terminal with building artifacts first
1. Clone repository via `git clone`
2. Open project in IntelliJ
3. Go to `File` > `Project Structure...` > `Artifacts`
4. Click a `plus sign` in the top left corner inside the right pane
5. Select `JAR` > `From modules with dependencies...`
6. Inside field called `Main Class` select a `Main.java` class
7. Select checkbox `extract to the target JAR`
8. Change `Directory for META-INF/MAINFEST.MF` from what is written by default to `<project_path>\src\main\resources`
9. Then right-click on the `personal_data_gernerator` folder (inside a tree-view on the right pane) and choose `Put into output root`
![intellij adding all dependencies to build](ss.png)

10. `Apply` and `OK`
11. Go into `Build` > `Build artifacts`
12. If artifacts were build correctly go into main project folder and run application by `java -jar out/artifacts/personal_data_generator_jar/personal_data_generator.jar`
        
         
         
