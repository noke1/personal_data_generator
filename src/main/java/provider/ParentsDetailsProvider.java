package provider;

import com.github.javafaker.Faker;
import pojo.ParentsDetails;

import java.util.Locale;

public class ParentsDetailsProvider {

    private final Faker faker;

    public ParentsDetailsProvider() {
        this.faker = new Faker(new Locale("pl"));
    }

    public ParentsDetails generateData(){
        return new ParentsDetails(
            generateFatherName(),
                generateMotherName(),
                generateMotherLastName()
        );
    }

    private String generateFatherName(){
        String fatherName;

        do {
            fatherName = faker.name().firstName();
        } while (fatherName.endsWith("a"));

        return fatherName;
    }

    private String generateMotherName(){
        String motherName;

        do {
            motherName = faker.name().firstName();
        } while (!motherName.endsWith("a"));

        return motherName;
    }

    private String generateMotherLastName(){
        String motherLastName;
        do {
            motherLastName = faker.name().lastName();
        } while (!motherLastName.endsWith("a"));

        return motherLastName;
    }
}
